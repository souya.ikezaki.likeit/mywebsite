

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.TodoDao;
import model.TodoBeans;
import model.UserBeans;

/**
 * Servlet implementation class UserListServlet
 */
@WebServlet("/list")
public class list extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public list() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//user情報
		HttpSession session = request.getSession();
		UserBeans user =(UserBeans)session.getAttribute("userInfo");
		
		if(user==null) {
			response.sendRedirect("login");
			return;
		}
		
		int id = user.getId();
		request.setAttribute("user",user);
		//TODO userのidを取得

		// userのidからtodo一覧情報を取得
		TodoDao todoDao = new TodoDao();
		List<TodoBeans> todoList = todoDao.findAll(id);

		// リクエストスコープにtodo一覧情報をセット
		request.setAttribute("todoList",todoList);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/list.jsp");
		dispatcher.forward(request, response);
		return;
		
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		// 入力されたtitleからtodoを検索
		String title = request.getParameter("title");
		
		request.setAttribute("title", title);		
		request.setAttribute("errMsg", "");
		
		TodoDao todoDao = new TodoDao();
		List<TodoBeans> todoList = new ArrayList<TodoBeans>();
		todoList = todoDao.search(title);
		request.setAttribute("todoList", todoList);
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/list.jsp");
		dispatcher.forward(request, response);
		return;
		
	}

}
