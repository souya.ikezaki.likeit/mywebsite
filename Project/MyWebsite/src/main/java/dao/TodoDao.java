package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.TodoBeans;

/**
 * ユーザテーブル用のDao
 * 
 * @author takano
 *
 */
public class TodoDao {


	//SELECT * FROM todo WHERE user_id = ?
	 //todo一覧表示
	public List<TodoBeans> findAll(int id) {
		Connection conn = null;
		PreparedStatement st = null;
		List<TodoBeans> todoList = new ArrayList<TodoBeans>();

		try {
			
			conn = DBManager.getConnection();

			st = conn.prepareStatement("select * from todo inner join user on user.id = todo.user_id where user.id = ?;");
			st.setInt(1, id);

			ResultSet rs = st.executeQuery();

			
			while (rs.next()) {

				TodoBeans todo = new TodoBeans();
				
					todo.setId(rs.getInt("todo_id"));
					todo.setTitle(rs.getString("title"));
					todo.setUserId(rs.getInt("user_id"));
					
				todoList.add(todo);
	
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
		
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return todoList;
	}
	
	
// todo一覧の検索機能
	public List<TodoBeans> search(String title) {
		Connection conn = null;
		List<TodoBeans> todoList = new ArrayList<TodoBeans>();
		try {
			conn = DBManager.getConnection();
			String sql = "SELECT * FROM todo WHERE";

			if(!title.equals("")){
				sql +=" title LIKE '%"+title+"%' ";
			}
			
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			
				while (rs.next()) {
					int id1 = rs.getInt("id");
					String title1 = rs.getString("title");
					int userId1 = rs.getInt("user_id");
					TodoBeans todo = new TodoBeans(id1, title1, userId1);

					todoList.add(todo);
				}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return todoList;
	}

//Todo更新メソッド
	public void updateTodo(String title) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();
			// INSERT文を準備
			String sql = "UPDATE todo SET title = ? WHERE todo_id = ? ";
			// INSERTを実行
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, title);

			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	

//findbyTodoIdメソッド
	public TodoDao findByTodoId(String todoId) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();
			// INSERT文を準備
			String sql = "DELETE FROM todo WHERE todo_id = ? ";
			// INSERTを実行
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, todoId);
			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return null;

	}


//新規登録
	public void createTodo(String title, int id) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();
			// INSERT文を準備
			String sql = "INSERT INTO todo(title,user_id) VALUES (?,?)";
			// INSERTを実行
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, title);
			pStmt.setInt(2, id);

			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public void deleteTodo(String todoId) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();
			// INSERT文を準備
			String sql = "DELETE FROM todo WHERE todo_id =?";
			// INSERTを実行
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, todoId);
			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}
		
	


//titleの重複チェック

	public TodoBeans findTitle(String title) {
		Connection conn = null;
		TodoBeans todoData = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM todo WHERE title = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, title);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			String title1 = rs.getString("title");
			todoData = new TodoBeans(title1);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return todoData;

	}
	
}