import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.TodoDao;
import dao.UserDao;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class update extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public update() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// TODO Auto-generated method stub
		
		String todoId = request.getParameter("todo_id");
		TodoDao todoDao = new TodoDao();
		TodoDao update = todoDao.findByTodoId(todoId);
		
		HttpSession session = request.getSession();
		UserDao user =(UserDao)session.getAttribute("userInfo");
		
		if(user==null) {
			response.sendRedirect("login");
			return;
		}
		
		request.setAttribute("update", update);
		request.setAttribute("todoId", update.getClass());//⇦未解決
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/update.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		// TODO Auto-generated method stub
		
		String id = request.getParameter("d");
		String title = request.getParameter("title");
		String userId= request.getParameter("user_id");
		
		if (title.equals("")) {
			request.setAttribute("title", title);
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/update.jsp");
			dispatcher.forward(request, response);
			return;
			}
		
		TodoDao todoDao = new TodoDao();
		todoDao.updateTodo(title);
		
		response.sendRedirect("UserListServlet");
	}

}
