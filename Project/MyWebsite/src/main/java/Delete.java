
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.TodoDao;
import model.UserBeans;

/**
 * Servlet implementation class UserDelate
 */
@WebServlet("/Delete")
public class Delete extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Delete() {
        super();
        // TODO Auto-generated constructor stub
    }
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String todoId = request.getParameter("todo_id");
		TodoDao todoDao = new TodoDao();
		
		HttpSession session = request.getSession();
		UserBeans user =(UserBeans)session.getAttribute("userInfo");
		int id = user.getId();
		
		todoDao.deleteTodo(todoId,id);
		
		response.sendRedirect("list");
		
	}

}
