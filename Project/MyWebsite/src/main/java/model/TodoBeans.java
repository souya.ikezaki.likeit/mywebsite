
package model;

import java.io.Serializable;

/**
 * Userテーブルのデータを格納するためのBeans
 * @author takano
 *
 */
public class TodoBeans implements Serializable {
	private int todoId;
	private String title;
	private int userId;
	

	// ログインセッションを保存するためのコンストラクタ
	public TodoBeans(int todoId, String name) {
		this.todoId = todoId;
	}

	// 全てのデータをセットするコンストラクタ
	public TodoBeans(int todoId, String title, int userId) {
		this.todoId = todoId;
		this.title = title;
		this.userId = userId;
	}

	public TodoBeans(String title1) {
		// TODO 自動生成されたコンストラクター・スタブ
	}

	

	public TodoBeans() {
		// TODO 自動生成されたコンストラクター・スタブ
	}

	public int getId() {
		return todoId;
	}
	public void setId(int id) {
		this.todoId = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}


}
