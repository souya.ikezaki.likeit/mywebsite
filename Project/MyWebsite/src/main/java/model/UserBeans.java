
package model;

import java.io.Serializable;

/**
 * Userテーブルのデータを格納するためのBeans
 * @author takano
 *
 */
public class UserBeans implements Serializable {
	private int id;
	private String name;
	private String email;
	private String password;
	

	// ログインセッションを保存するためのコンストラクタ
	public UserBeans(int id, String name) {
		this.id = id;
		this.name = name;
	}

	// 全てのデータをセットするコンストラクタ
	public UserBeans(int id, String name, String email, String password) {
		this.id = id;
		this.name = name;
		this.email = email;
		this.password = password;
	}


	public UserBeans(int id2) {
		// TODO 自動生成されたコンストラクター・スタブ
		this.id = id2;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}




}
