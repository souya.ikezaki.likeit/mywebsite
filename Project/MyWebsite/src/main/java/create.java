

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.TodoDao;
import model.TodoBeans;
import model.UserBeans;
/**
* Servlet implementation class NewUser
 */
@WebServlet("/create")
public class create extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public create() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		UserBeans user =(UserBeans)session.getAttribute("userInfo");
		
		if(user==null) {
			response.sendRedirect("login");
			return;
		}
		
		request.setAttribute("user",user);
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/create.jsp");
		dispatcher.forward(request, response);
	
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		// TODO Auto-generated method stub
		
		String title = request.getParameter("title");
		
		TodoDao todoDao = new TodoDao();
		//TODO loginIdが重複した時の処理
        TodoBeans titleData = todoDao.findTitle(title);
		if (!(titleData==null)) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/create.jsp");
			dispatcher.forward(request, response);
			return;
		}
		
		//入力漏れの場合
		if (title.equals("")) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/create.jsp");
			dispatcher.forward(request, response);
			return;
			}

		HttpSession session = request.getSession();
		UserBeans user =(UserBeans)session.getAttribute("userInfo");
		int id = user.getId();
		
		todoDao.createTodo(title,id);
		
		response.sendRedirect("list");

		
		
		
	}


}
